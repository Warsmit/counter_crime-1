from django.db import models
from weapon.models import Weapon
from stock.models import Stock
# from django.db.models.signals import m2m_changed
from tier_model.models import TierModel
class WeaponRoom(models.Model):
    """
    Model for storing information about weapon rooms
        fields:
            name -> this is title of the weapon room
            weapons -> weapon name
            tier -> this is tier where weapon room located
            created -> date and time of creation
            updated -> date and time of updation

    """
    class Meta:
        db_table = "WeaponRoom"
        verbose_name = "Оружейная комната"
        verbose_name_plural = "Оружейные комнаты"
        permissions = (
            ("view_weapon_room", "Can view Оружейные комнаты"),
        )

    name = models.CharField(
        verbose_name="Название оружейной комнаты", blank=False, max_length=200, null=False)
    weapons = models.ManyToManyField(
        Weapon, default=None, verbose_name="Наименование Оружия",  blank=True,)
    tier = models.ForeignKey('tier_model.TierModel', related_name="Tier", verbose_name="Тир", on_delete=models.SET_NULL, blank=True, null=True)
    created = models.DateTimeField(
        verbose_name="Создано", auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(
        verbose_name="Обновлено", auto_now_add=False, auto_now=True)

    def __str__(self):
        return '{NAME}'.format(
            NAME=self.name,
        )

#     def m2m_changed(sender, **kwargs):
#         if kwargs['action'] == "post_remove":
#             tiers = TierModel.objects.all()
#             for tier in tiers:
#                 if kwargs['instance'].tier.pk == str(tier.pk):
#                     tier.stock.weapons.add(kwargs['model'])
#         if kwargs['action'] == "pre_add":
#             tiers = TierModel.objects.all()
#             for tier in tiers:
#                 if kwargs['instance'].tier.pk == str(tier.pk):
#                     tier.stock.weapons.remove(kwargs['model'])

# m2m_changed.connect(WeaponRoom.m2m_changed, sender=WeaponRoom.weapons.through )