from django.contrib import admin
from .models import WeaponRoom
from tier_model import models

class WeaponRoomAdmin(admin.ModelAdmin):
    list_display = [field.name for field in WeaponRoom._meta.fields]
    readonly_fields = ['tier']

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "weapons":
            query_set = []
            for tier in models.TierModel.objects.all():
                for weapon_room in WeaponRoom.objects.all():
                    if weapon_room.tier.pk == tier.pk:
                        if tier.stock is not None:
                            query_set = tier.stock.weapons
                            kwargs["queryset"] = query_set
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    class Meta:
        model = WeaponRoom

admin.site.register(WeaponRoom, WeaponRoomAdmin)