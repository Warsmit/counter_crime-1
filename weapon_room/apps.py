from django.apps import AppConfig


class WeaponRoomConfig(AppConfig):
    name = 'weapon_room'
