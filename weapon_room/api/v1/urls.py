from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (WeaponRoomList, WeaponRoomDetail)

urlpatterns = [
    url(r'^weapon_rooms/$', WeaponRoomList.as_view(), name='weaponroom-list'),
    url(r'^weapon_rooms/(?P<pk>[0-9]+)$', WeaponRoomDetail.as_view(), name='weaponroom-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)