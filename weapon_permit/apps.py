from django.apps import AppConfig


class WeaponPermitConfig(AppConfig):
    name = 'weapon_permit'
