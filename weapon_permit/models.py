from django.db import models
from django.db.models.signals import post_delete
from django.core.files.storage import FileSystemStorage
from utils.rename_image import rename_weapon_permit_images
from client_model.models import  ClientModel
from counter_crime.settings import IMAGE_STORAGE

class WeaponPermit(models.Model):
    """
    Model for storing information about weapon permits
        fields:
            image -> weapon permit image
            client -> client (ClientModel)
            number -> application number
            issuer_date -> date of issue of the weapon permit
            created -> date and time of creation
        additional funcs:
            delete_image: remove weapon permit image

    """
    class Meta:
        db_table = 'WeaponPermit'
        verbose_name = 'Разрешение на оржуе'
        verbose_name_plural = 'Разрешения на оружие'
        permissions = (
            ('view_weapon_permit', 'Can view Разрешения на оружие'),
        )

    image = models.ImageField(verbose_name="Фото разрешения на оружие", storage=IMAGE_STORAGE, max_length=500, blank=True, upload_to=rename_weapon_permit_images)
    client = models.ForeignKey(ClientModel, on_delete=models.CASCADE, blank=False)
    number = models.SmallIntegerField(verbose_name="Номер заявления", blank=False, null=False )
    issuer_date = models.DateField(verbose_name="Дата выдачи", blank=False, null=False)
    created = models.DateTimeField(verbose_name='Created', auto_now_add=True, auto_now=False)

    def __str__(self):
        return '{USER} {NUMBER} {IMAGE} {ISSUER_DATE}'.format(
            USER=self.user,
            NUMBER=self.number,
            IMAGE=self.pk,
            ISSUER_DATE=self.issuer_date, 
        )

    @staticmethod
    def detete_image(sender, instance, *args, **kwargs):
        if instance.image:
            print(instance.image.path)
            if os.path.isfile(instance.image.path):
                os.remove(instance.image.path)

post_delete.connect(sender=WeaponPermit, receiver=WeaponPermit.detete_image)
