from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    WeaponPermitList,
    WeaponPermitDetail,
)


urlpatterns = [
    url(r'^weapon_permits/$', WeaponPermitList.as_view(), name='weaponpermit-list'),
    url(r'^weapon_permits/(?P<pk>[0-9]+)$', WeaponPermitDetail.as_view(), name='weaponpermit-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
