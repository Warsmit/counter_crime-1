from .serializers import WeaponPermitSerializer
from weapon_permit.models import WeaponPermit
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)
from rest_framework.parsers import FileUploadParser

class WeaponPermitList(generics.ListCreateAPIView):
    queryset = WeaponPermit.objects.all()
    serializer_class = WeaponPermitSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    # filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class WeaponPermitDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = WeaponPermit.objects.all()
    serializer_class = WeaponPermitSerializer
    parser_classes = (FileUploadParser,)

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]

    # def post(self, request, *args, **kwargs):
    #     file_serializer = ClientModelSerializer(data=request.data)

    #     if file_serializer.is_valid():
    #         file_serializer.save()
    #         return Response(file_serializer.data, status=status.HTTP_201_CREATED)
    #     else:
    #         return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)