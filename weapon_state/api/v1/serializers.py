from rest_framework import serializers
from weapon_state.models import WeaponState

class WeaponStateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeaponState
        fields = '__all__'