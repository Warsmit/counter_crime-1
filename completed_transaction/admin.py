from django.contrib import admin
from .models import CompletedTransaction

class CompletedTransactionAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CompletedTransaction._meta.fields]

    class Meta:
        model = CompletedTransaction

admin.site.register(CompletedTransaction, CompletedTransactionAdmin)
