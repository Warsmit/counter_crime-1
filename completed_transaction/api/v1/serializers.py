from rest_framework import serializers
from completed_transaction.models import CompletedTransaction

class CompletedTransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CompletedTransaction
        fields = '__all__'
