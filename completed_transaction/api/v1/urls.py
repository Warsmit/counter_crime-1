from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    CompletedTransactionList,
    CompletedTransactionDetail,
)


urlpatterns = [
    url(r'^completed_transactions/$', CompletedTransactionList.as_view(), name='completedtransactions-list'),
    url(r'^completed_transactions/(?P<pk>[0-9]+)$', CompletedTransactionDetail.as_view(), name='completedtransaction-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
