
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "counter_crime.settings")
django.setup()
from counter_crime.settings import PERMISSION_CLASSES, ADMIN_PHONE_NUMBER
from user.models import User
from django.contrib.auth.models import (
    # User,
    Group,
    Permission,
)
groups = [
    {"name": PERMISSION_CLASSES['admin']},
    {"name": PERMISSION_CLASSES['moderator']},
    {"name": PERMISSION_CLASSES['client']},
]

for group in groups:
    Group.objects.create(**group)


admin = Group.objects.get(name=PERMISSION_CLASSES['admin'])
moderator = Group.objects.get(name=PERMISSION_CLASSES['moderator'])
client = Group.objects.get(name= PERMISSION_CLASSES['client'])


# admin_group_permissions = [
#     "view_category",
#     "change_category",
#     "add_category",
#     "delete_category",
#     "view_categorytype",
#     "change_categorytype",
#     "add_categorytype",
#     "delete_categorytype",
#     "view_city",
#     "change_city",
#     "add_city",
#     "delete_city",
#     "view_currency",
#     "change_currency",
#     "add_currency",
#     "delete_currency",
#     "view_clientprofile",
#     "change_clientprofile",
#     "add_clientprofile",
#     "delete_clientprofile",
#     "view_clientregistrationrequest",
#     "change_clientregistrationrequest",
#     "add_clientregistrationrequest",
#     "delete_clientregistrationrequest",
#     "view_guideprofile",
#     "change_guideprofile",
#     "add_guideprofile",
#     "delete_guideprofile",
#     "view_guideregistrationrequest",
#     "change_guideregistrationrequest",
#     "add_guideregistrationrequest",
#     "delete_guideregistrationrequest",
#     "view_language",
#     "change_language",
#     "add_language",
#     "delete_language",
#     "view_point",
#     "change_point",
#     "add_point",
#     "delete_point",
#     "view_route",
#     "change_route",
#     "add_route",
#     "delete_route",
#     # "view_routeimage",
#     # "change_routeimage",
#     # "add_routeimage",
#     # "delete_routeimage",
#     "view_shortroute",
#     "change_shortroute",
#     "add_shortroute",
#     "delete_shortroute",
#     "view_routetransaction",
#     "add_routetransaction",
#     "change_routetransaction",
#     "delete_routetransaction",
# ]

# moderator_group_permissions = [
#     "view_category",
#     "change_category",
#     "add_category",
#     "view_categorytype",
#     "change_categorytype",
#     "add_categorytype",
#     "view_city",
#     "change_city",
#     "add_city",
#     "view_currency",
#     "change_currency",
#     "add_currency",
#     "view_clientprofile",
#     "change_clientprofile",
#     "view_clientregistrationrequest",
#     "change_clientregistrationrequest",
#     "view_guideprofile",
#     "change_guideprofile",
#     "view_guideregistrationrequest",
#     "change_guideregistrationrequest",
#     "view_language",
#     "change_language",
#     "add_language",
#     "view_point",
#     "change_point",
#     "add_point",
#     "view_route",
#     "change_route",
#     "add_route",
#     # "view_routeimage",
#     # "change_routeimage",
#     # "add_routeimage",
#     # "delete_routeimage",
#     "view_shortroute",
#     "view_routetransaction",
#     "add_routetransaction",
#     "change_routetransaction",
# ]

# guide_group_permissions = [
#     "view_category",
#     'view_clientprofile',
#     "view_categorytype",
#     "view_city",
#     "view_currency",
#     "view_clientprofile",
#     # "change_clientprofile",
#     # "view_clientregistrationrequest",
#     # "change_clientregistrationrequest",
#     # "add_clientregistrationrequest",
#     # "delete_clientregistrationrequest",
#     "view_guideprofile",
#     # "view_guideregistrationrequest",
#     # "change_guideregistrationrequest",
#     # "add_guideregistrationrequest",
#     # "delete_guideregistrationrequest",
#     "view_language",
#     "view_point",
#     "add_point",
#     "view_route",
#     "change_route",
#     "add_route",
#     "delete_route",
#     # "view_routeimage",
#     # "change_routeimage",
#     # "add_routeimage",
#     # "delete_routeimage",
#     "view_shortroute",
#     "view_routetransaction",
#     "add_routetransaction",
# ]

# client_group_permissions = [
#     "view_category",
#     'view_clientprofile',
#     "view_categorytype",
#     "view_city",
#     "view_currency",
#     # "view_clientregistrationrequest",
#     # "change_clientregistrationrequest",
#     # "add_clientregistrationrequest",
#     # "delete_clientregistrationrequest",
#     "view_guideprofile",
#     # "view_guideregistrationrequest",
#     # "change_guideregistrationrequest",
#     # "add_guideregistrationrequest",
#     # "delete_guideregistrationrequest",
#     "view_language",
#     "view_point",
#     "view_route",
#     # "view_routeimage",
#     # "change_routeimage",
#     # "add_routeimage",
#     # "delete_routeimage",
#     "view_shortroute",
#     # "view_routetransaction",
#     # "add_routetransaction",
#     # "change_routetransaction",
#     # "delete_routetransaction",
# ]

# # print(Permission.objects.get(codename="view_routetransaction"))

admin_group_permissions = []
moderator_group_permissions = []
client_group_permissions = []


admin.permissions.clear()
client.permissions.clear()
moderator.permissions.clear()


for permission in admin_group_permissions:
    print(permission)
    permission = Permission.objects.get(codename=permission)
    admin.permissions.add(permission)

for permission in moderator_group_permissions:
    permission = Permission.objects.get(codename=permission)
    moderator.permissions.add(permission)

for permission in client_group_permissions:
    permission = Permission.objects.get(codename=permission)
    client.permissions.add(permission)

user_admin = User.objects.get(pk=1)
user_admin.phone_number = ADMIN_PHONE_NUMBER
user_admin.groups.add(admin)
user_admin.save()
