from django.contrib import admin
from .models import ActiveWorkPeriod

class ActiveWorkPeriodAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ActiveWorkPeriod._meta.fields]

    class Meta:
        model = ActiveWorkPeriod

admin.site.register(ActiveWorkPeriod, ActiveWorkPeriodAdmin)