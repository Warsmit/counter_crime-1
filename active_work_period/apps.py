from django.apps import AppConfig


class ActiveWorkPeriodConfig(AppConfig):
    name = 'active_work_period'
