from .serializers import ActiveWorkPeriodSerializer
from active_work_period.models import ActiveWorkPeriod
from completed_transaction.models import CompletedTransaction
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from django.views.decorators.csrf import csrf_exempt
from staff.models import Staff
from completed_work_period.models import CompletedWorkPeriod
from datetime import datetime
from pytz import timezone
from counter_crime.settings import TIME_ZONE
import json

class ActiveWorkPeriodList(generics.ListCreateAPIView):
    queryset = ActiveWorkPeriod.objects.all()
    serializer_class = ActiveWorkPeriodSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class ActiveWorkPeriodDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ActiveWorkPeriod.objects.all()
    serializer_class = ActiveWorkPeriodSerializer

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]


@csrf_exempt
@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated,))
def close_work_period(request, format=None):
    
    if request.method == 'POST':
        try:
            data = request.data
        except json.JSONDecodeError:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            active_work_period = ActiveWorkPeriod.objects.get(pk=data["pk"])
        except(ActiveWorkPeriod.DoesNotExist):
            return Response(status=status.HTTP_404_NOT_FOUND)
        completed_work_period = CompletedWorkPeriod.objects.create(
            working_time=str(datetime.now(timezone(TIME_ZONE)) - active_work_period.created)
        )
        for staff in active_work_period.staff.all():
            completed_work_period.staff.add(staff)
        for transaction in active_work_period.active_transaction.all():
            completed_transacton = CompletedTransaction.objects.create(
                        client=transaction.client,
                        weapon=transaction.weapon
                    )
            completed_transacton.save()
            transaction.tier.active_work_period.completed_transaction.add(completed_transacton)
            transaction.tier.active_work_period.active_transaction.remove(transaction)
            transaction.delete()
        for completed_transaction in active_work_period.completed_transaction.all():
            completed_work_period.completed_transaction.add(completed_transaction)
        completed_work_period.save()
        active_work_period.delete()
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_403_FORBIDDEN)
