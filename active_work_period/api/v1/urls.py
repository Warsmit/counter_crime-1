from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (ActiveWorkPeriodList, ActiveWorkPeriodDetail)

urlpatterns = [
    url(r'^active_work_periods/$', ActiveWorkPeriodList.as_view(), name='activeworkperiod-list'),
    url(r'^active_work_periods/(?P<pk>[0-9]+)$', ActiveWorkPeriodDetail.as_view(), name='activeworkperiod-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
