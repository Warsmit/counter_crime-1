from rest_framework import serializers
from active_work_period.models import ActiveWorkPeriod

class ActiveWorkPeriodSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ActiveWorkPeriod
        fields = '__all__'