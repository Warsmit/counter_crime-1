# -*- mode: python ; coding: utf-8 -*-
import sys
import os
import re
from pathlib import Path
from PyInstaller.utils.hooks import collect_submodules

DEBUG = True

lib = Path("")
hidden_imports = []
hidden_imports +=collect_submodules('pillow')
hidden_imports_str = [                  
                  'django.contrib.admin.apps', 
                  'django.contrib.auth.apps', 
                  'django.contrib.contenttypes.apps', 
                  'django.contrib.sessions.apps', 
                  'django.contrib.messages.apps', 
                  'django.contrib.staticfiles.apps', 
                  'django.contrib.messages.middleware', 
                  'django.contrib.sessions.middleware', 
                  'allauth.account.apps',
                  'rest_framework.apps',
                  'rest_framework.authtoken.apps',
                  'rest_framework.authentication',
                  'rest_framework.templates',
                  'rest_framework.renderers',
                  'rest_framework.metadata',
                  'rest_framework',
                  'django',
                  'django.template.smartif',
                  'rest_framework.negotiation',
                  'django.utils.lorem_ipsum',
                  'django.contrib.sessions.serializers', 
                  'django.template.loaders', 
                  'django.contrib.auth.context_processors', 
                  'django.contrib.messages.context_processors'
                  'rest_auth.apps',
                  'django_filters.apps'
                  'django.contrib.sites.management',
                  'med_certificate',
                  'weapon_permit',
                  'client_model',
                  'user',
                  'pasport',
                  'weapon_type',
                  'weapon',
                  'registration_place',
                  'gender',
                  'stock',
                  'tier_model',
                  'target_model',
                  'weapon_state',
                  'staff',
                  'django.contrib.admin.templates',
                  'corsheaders',
                  'staff_position',
                  'active_work_period',
                  'completed_work_period',
                  'django.core.management.commands.runserver',
                  'django.template',
                  'rest_framework_swagger.views',
                  'Pillow',
                ]
hidden_imports += lib.glob("client_model/**/*.py")
hidden_imports += lib.glob("user/**/*.py")
hidden_imports += lib.glob("pasport/**/*.py")
hidden_imports += lib.glob("weapon_type/**/*.py")
hidden_imports += lib.glob("weapon/**/*.py")
hidden_imports += lib.glob("registration_place/**/*.py")
hidden_imports += lib.glob("completed_transaction/**/*.py")
hidden_imports += lib.glob("weapon_room/**/*.py")
hidden_imports += lib.glob("active_transaction/**/*.py")
hidden_imports += lib.glob("ammo_type/**/*.py")
hidden_imports += lib.glob("med_certificate/**/*.py")
hidden_imports += lib.glob("gender/**/*.py")
hidden_imports += lib.glob("stock/**/*.py")
hidden_imports += lib.glob("tier_model/**/*.py")
hidden_imports += lib.glob("target_model/**/*.py")
hidden_imports += lib.glob("weapon_state/**/*.py")
hidden_imports += lib.glob("staff/**/*.py")
hidden_imports += lib.glob("staff_position/**/*.py")
hidden_imports += lib.glob("active_work_period/**/*.py")
hidden_imports += lib.glob("completed_work_period/**/*.py")
hidden_imports += lib.glob("weapon_permit/**/*.py")
hidden_imports += lib.glob("target_model/**/*.py")
hidden_imports = list(str(x) for x in hidden_imports)
for index, path in enumerate(hidden_imports):
    path = re.sub(r"lib(\\|\/)", "", path)

    if "__init__.py" in path:
        path = re.sub(r"(\\|\/)__init__.py", "", path)
    else:
        path = re.sub(r"\.py", "", path)

    hidden_imports[index] = ".".join(re.split(r"\\|\/", path))

hidden_imports += hidden_imports_str

block_cipher = None


a = Analysis(['manage.py'],
             pathex=['C:\\my\\Projects\\python\\counter_crime'],
             binaries=[],
             datas=[],
             hiddenimports=hidden_imports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='manage',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='manage')
