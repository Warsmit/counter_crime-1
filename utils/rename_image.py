import uuid

def rename_image(instance, filename, folder = ''):
    data = filename.split('.')
    return '{}/{}.{}'.format(folder, str(uuid.uuid4())[:30], data[-1])

def rename_client_images(instance, filename):
    return rename_image(instance, filename, 'client_images')

def rename_med_sertificate_images(instance, filename):
    return rename_image(instance, filename, 'med_sertificate_images')

def rename_staff_images(instance, filename):
    return rename_image(instance, filename, 'staff')

def rename_weapon_permit_images(instance, filename):
    return rename_image(instance, filename, 'weapon_permit')

