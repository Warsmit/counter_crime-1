from django.db import models
from weapon_type.models import WeaponType
from ammo_type.models import AmmoType
from weapon_state.models import WeaponState

class Weapon(models.Model):
    """
    Model for storing information about weapons
        fields:
            weapon_types -> type of weapon
            weapon_name -> weapon name
            serial_number -> serial number
            ammo_type -> type of ammo
            shots -> shots count
            state -> weapon state
            created -> date and time of creation
            updated -> date and time of updation

    """
    class Meta:
        db_table = "Weapon"
        verbose_name = "Оружие"
        verbose_name_plural = "Оружия"
        permissions = (
            ("view_weapons", "Can view Оружия"),
        )

    weapon_types = models.ForeignKey(WeaponType, verbose_name="Тип Оружия", on_delete=models.SET_NULL, blank=False, null=True, max_length=100)
    weapon_name =  models.CharField(verbose_name="Наименование Оружия", blank=False, null=False, max_length=100)
    serial_number = models.CharField(verbose_name="Серийный Номер", blank=False, null=False, max_length=100)
    ammo_type = models.ForeignKey(AmmoType, default=None, on_delete=models.SET_NULL, verbose_name="Тип Боеприпасов", blank=True, null=True)
    shots = models.PositiveIntegerField(verbose_name="Настрел", default=0, blank=False, null=False)
    state = models.ForeignKey(WeaponState, verbose_name="Состояние", blank=False, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(verbose_name="Создано", auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(verbose_name="Обновлено", auto_now_add=False, auto_now=True)

    def __str__(self):
        return '{NAME} {SERIAL_NUMBER} {SHOTS} выстрелов {STATE} '.format(
            NAME=self.weapon_name,
            SERIAL_NUMBER=self.serial_number,
            SHOTS=self.shots,
            STATE=self.state,
        )
