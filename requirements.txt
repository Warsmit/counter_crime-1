django==2.2.14
django-fernet-fields==0.6
djangorestframework==3.11.0
django-rest-auth==0.9.5
django-allauth==0.42.0
django-rest-swagger==2.2.0
django-filter==2.3.0
django-cors-headers==3.4.0
Pillow==7.2.0
pyinstaller==4.0
pywin32==228