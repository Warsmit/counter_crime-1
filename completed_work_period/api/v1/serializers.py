from rest_framework import serializers
from completed_work_period.models import CompletedWorkPeriod

class CompletedWorkPeriodSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CompletedWorkPeriod
        fields = '__all__'
