from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    CompletedWorkPeriodList,
    CompletedWorkPeriodDetail,
)


urlpatterns = [
    url(r'^completed_work_periods/$', CompletedWorkPeriodList.as_view(), name='completedworkperiods-list'),
    url(r'^completed_work_periods/(?P<pk>[0-9]+)$', CompletedWorkPeriodDetail.as_view(), name='completedworkperiods-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
