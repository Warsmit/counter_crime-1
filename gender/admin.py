from django.contrib import admin
from .models import Gender

class GenderAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Gender._meta.fields]

    class Meta:
        model = Gender

admin.site.register(Gender, GenderAdmin)
