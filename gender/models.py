from django.db import models
from fernet_fields import EncryptedCharField

# Create your models here.

class Gender(models.Model):
    """
    Model for storing gender information
        fields:
            name -> gender

    """
    class Meta:
        db_table = "Gender"
        verbose_name = "Пол"
        verbose_name_plural = "Пол"
        permissions = (
            ("view_genders", "Can view Пол"),
        )
    
    name = EncryptedCharField(verbose_name="Пол", blank=False, null=False, max_length=3 )

    def __str__(self):
        return self.name