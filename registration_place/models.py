from django.db import models
from fernet_fields import (
    EncryptedCharField,
    EncryptedDateField,
    EncryptedIntegerField

    )
class RegistrationPlace(models.Model):
    """
    Model for storing information about clients location
        fields:
            region -> region
            township -> name of the settlement
            street -> street name
            house_number -> house number
            buildings -> building number
            apartment -> apartment number
            created -> date and time of creation
            updated -> date and time of updation

    """
    class Meta:
        db_table = "RegistrationPlace"
        verbose_name = "Место Регистрации"
        verbose_name_plural = "Места Регистрации"
        permissions = (
            ("view_registration_place", "Can view Место Регистрации"),
        )

    region =  EncryptedCharField(verbose_name="Область", blank=False, null=False, max_length=200)
    township =  EncryptedCharField(verbose_name="Город Поселок", blank=False, null=False, max_length=200)
    street = EncryptedCharField(verbose_name="Улица", blank=False, null=False, max_length=200)
    house_number = EncryptedIntegerField(verbose_name="Номер Дома", blank=False, null=False)
    building = EncryptedIntegerField(verbose_name="Корпус", blank=True, null=True, default=None)
    apartment = EncryptedIntegerField(verbose_name="Номер Квартиры", blank=False, null=False)
    created = models.DateTimeField(verbose_name="Создано", auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(verbose_name="Обновлено", auto_now_add=False, auto_now=True)

    def __str__(self):
        return '{NAME}'.format(
            NAME=self.region,
        )