from rest_framework import serializers
from registration_place.models import RegistrationPlace

class RegistrationPlaceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RegistrationPlace
        fields = '__all__'