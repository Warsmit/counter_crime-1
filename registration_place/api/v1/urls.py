from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (RegistrationPlaceList, RegistrationPlaceDetail)

urlpatterns = [
    url(r'^registration_places/$', RegistrationPlaceList.as_view(), name='registrationplace-list'),
    url(r'^registration_places/(?P<pk>[0-9]+)$', RegistrationPlaceDetail.as_view(), name='registrationplace-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)