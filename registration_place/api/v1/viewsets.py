from .serializers import RegistrationPlaceSerializer
from registration_place.models import RegistrationPlace
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)


class RegistrationPlaceList(generics.ListCreateAPIView):
    queryset = RegistrationPlace.objects.all()
    serializer_class = RegistrationPlaceSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class RegistrationPlaceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = RegistrationPlace.objects.all()
    serializer_class = RegistrationPlaceSerializer

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]