from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    PasportList,
    PasportDetail,
)


urlpatterns = [
    url(r'^pasports/$', PasportList.as_view(), name='pasport-list'),
    url(r'^pasports/(?P<pk>[0-9]+)$', PasportDetail.as_view(), name='pasport-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
