from django.db import models
from user.models import User
from staff_position.models import StaffPosition
from django.db.models.signals import pre_save, post_delete
from django.contrib.auth.models import Group
from rest_framework.exceptions import PermissionDenied
from utils.group_validator import StaffValidator
from counter_crime.settings import PERMISSION_CLASSES
from fernet_fields import EncryptedCharField
from counter_crime.settings import IMAGE_STORAGE
from utils.rename_image import rename_staff_images

class Staff(models.Model):
    """
    Model for storing information about staff
        fields:
            user -> user (OneToOne)
            phone_number -> phone number
            staff_position -> employee's position
            image -> employee`s photo
        additional funcs:
            pre_save: checks possibility of creating staff by the user group
            delete_image: delete employee`s image

    """
    class Meta:
        db_table = "Staff"
        verbose_name = "Сотрудник"
        verbose_name_plural = "Сотрудники"
        permissions = (
            ("view_staffs", "Can view Сотрудники"),
        )

    user = models.OneToOneField(
        User, verbose_name="User", on_delete=models.CASCADE, blank=False, null=False, validators=[StaffValidator()])
    staff_position = models.ForeignKey(StaffPosition, verbose_name="Должность", on_delete=models.SET_NULL, null=True, blank=True)
    image = models.ImageField(verbose_name="Фото Сотрудника", storage=IMAGE_STORAGE, max_length=500, blank=True, upload_to=rename_staff_images)

    @staticmethod
    def pre_save(sender, instance, *args, **kwargs):
        client_group = Group.objects.get(name= PERMISSION_CLASSES['client'])
        if instance.user.groups.all()[0] == client_group:
            raise PermissionDenied("Клиенты не могут быть сотрудниками")
        instance.user.save()


    def __str__(self):
        return '{} {} {} {} {}'.format(self.user.email, self.user.last_name, self.user.first_name,  self.user.second_name, self.staff_position, )

    @staticmethod
    def detete_image(sender, instance, *args, **kwargs):
        if instance.image:
            print(instance.image.path)
            if os.path.isfile(instance.image.path):
                os.remove(instance.image.path)

post_delete.connect(sender=Staff, receiver=Staff.detete_image)
pre_save.connect(Staff.pre_save, sender=Staff)