from django.contrib import admin
from .models import Staff
from django.contrib.auth.models import Group
from user.models import User
from django.utils.safestring import mark_safe
class StaffAdmin(admin.ModelAdmin):
    list_display = ["user", "staff_position", "get_image"]

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "user":
            kwargs['queryset'] =  User.objects.filter(groups=Group.objects.filter(name="Moderator")[0].pk)
            print(kwargs['queryset'])
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
    class Meta:
        model = Staff

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="220" height"220"')

admin.site.register(Staff, StaffAdmin)
