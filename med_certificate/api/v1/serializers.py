from rest_framework import serializers
from med_certificate.models import MedCertificate

class MedCertificateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MedCertificate
        fields = '__all__'

    image = serializers.ImageField(required=False,max_length=None, use_url=True)