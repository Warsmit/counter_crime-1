from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    MedCertificateList,
    MedCertificateDetail,
)


urlpatterns = [
    url(r'^med_certificates/$', MedCertificateList.as_view(), name='medcertificate-list'),
    url(r'^med_certificates/(?P<pk>[0-9]+)$', MedCertificateDetail.as_view(), name='medcertificate-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
