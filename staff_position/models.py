from django.db import models
from fernet_fields import EncryptedCharField
# Create your models here.

class StaffPosition(models.Model):
    """
    Model for storing employee`s positions
        fields:
            name -> this is job title

    """
    class Meta:
        db_table = "StaffPosition"
        verbose_name = "Должность"
        verbose_name_plural = "Должности"
        permissions = (
            ("view_staff_position", "Can view Должности"),
        )
    
    name = EncryptedCharField(verbose_name="Должность", blank=False, null=False, max_length=200 )

    def __str__(self):
        return self.name