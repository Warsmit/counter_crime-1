from rest_framework import serializers
from staff_position.models import StaffPosition

class StaffPositionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = StaffPosition
        fields = '__all__'
