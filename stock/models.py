from django.db import models
from weapon.models import Weapon
from ammo_type.models import AmmoType
# from django.db.models.signals import m2m_changed
from tier_model.models import TierModel

class Stock(models.Model):
    """
    Model for storing information about stocks
        fields:
            name -> this is title of this stock
            weapon -> array of weapons on this stock (ManyToMany)
            ammo -> array of ammo on this stock (ManyToMany)
            tier -> this is tier where stock located
            created -> date and time of creation
            updated -> date and time of updation

    """
    class Meta:
        db_table = "Stock"
        verbose_name = "Склад"
        verbose_name_plural = "Склады"
        permissions = (
            ('view_stocks', "Can view Склады"),
        )

    name = models.CharField(
        verbose_name="Наименование склада", max_length=200, blank=False, null=False)
    weapons = models.ManyToManyField(
        Weapon, verbose_name="Оружия на складе", blank=True)
    ammo = models.ManyToManyField(
        AmmoType, verbose_name="Боеприпасы", blank=True)
    tier = models.ForeignKey('tier_model.TierModel', related_name="TierModel", verbose_name="Тир", on_delete=models.SET_NULL, blank=True, null=True)
    created = models.DateTimeField(
        verbose_name="Создано", auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(
        verbose_name="Обновлено", auto_now_add=False, auto_now=True)

    def __str__(self):
        return '{NAME} '.format(
            NAME=self.name
        )

    # def m2m_changed(sender, **kwargs):
    #     if kwargs['action'] == "pre_remove":
    #         tiers = TierModel.objects.all()
    #         for tier in tiers:
    #             print(kwargs['instance'].tier.pk)
    #             print(str(tier.pk))
    #             if str(kwargs['instance'].tier.pk) == str(tier.pk):
    #                 if hasattr(tier.weapon_room, 'weapons'):
    #                     tier.weapon_room.weapons.remove(kwargs['model'])

# m2m_changed.connect(Stock.m2m_changed, sender=Stock.weapons.through )