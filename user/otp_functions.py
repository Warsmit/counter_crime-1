import random
from rest_framework.decorators import permission_classes
from rest_framework import permissions
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from user.models import PhoneOTP
from user.models import User
from rest_framework.response import Response
from rest_framework import status, exceptions


def send_otp_phone(phone, key):
    otp_key = key
    name = phone

    #link = f'https://2factor.in/API/R1/?module=TRANS_SMS&apikey=YOUR_API_KEY&to={phone}&from=YOUR_TEMPLATE&templatename=YOUR_TEMPLATE_NAME&var1={name}&var2={otp_key}'

    #result = requests.get(link, verify=False)


def send_otp_this(phone_number):
    print(phone_number)
    phone_obj = User.objects.filter(phone_number=phone_number)

    key = random.randint(99999999, 999999999)
    print(key)

    phone_obj = PhoneOTP.objects.filter(phone_number=phone_number)
    if phone_obj.exists():
        phone_obj = phone_obj.first()
        count = phone_obj.count
        phone_obj.otp = key
        phone_obj.save()
        send_otp_phone(phone_number, key)

       # if count < 6:
        #     phone_obj.count = count + 1
        # else:
        #     raise exceptions.NotFound('OTP NOT FOUND')
    else:

        PhoneOTP.objects.create(
            phone_number=phone_number,
            otp=key,
            count=1
        )
        send_otp_phone(phone_number, key)


@csrf_exempt
@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def otp_match(request):
    if request.method == 'POST':
        try:
            data = request.data
        except json.JSONDecodeError:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    print(data)
    phone_number = data['phone']
    otp_user = data['otp']
    if phone_number and otp_user:
        phone_obj = PhoneOTP.objects.filter(phone_number=phone_number)
        user = User.objects.filter(phone_number=phone_number)
        if phone_obj.exists() and user.exists():
            phone_obj = phone_obj.first()
            user = user.first()
            otp_actual = phone_obj.otp
            if int(otp_actual) == int(otp_user):
                phone_obj.logged = True
                user.is_active = True
                user.save()
                phone_obj.save()
                return Response(status=status.HTTP_200_OK, data={})
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)
