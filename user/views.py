from rest_framework import serializers, exceptions
from django.contrib.auth.models import Group
from rest_auth.registration.serializers import RegisterSerializer
from rest_auth.serializers import LoginSerializer
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from rest_framework import status
from rest_auth.views import LoginView
from rest_auth.registration.views import RegisterView
from rest_framework.response import Response
from user.models import User, PhoneOTP
from counter_crime.settings import PERMISSION_CLASSES
from allauth.account import app_settings
from counter_crime.settings import ACCOUNT_PHONE_NUMBER_REQUIRED
from user.otp_functions import send_otp_this

class ExtendedLoginSerialiser(LoginSerializer):
    phone_number = serializers.CharField(
        required=ACCOUNT_PHONE_NUMBER_REQUIRED, allow_blank=True)

    def _validate_username_email_phone_number(self, username, email, password, phone_number):
        user = None

        if email and password:
            user = self.authenticate(email=email, password=password)
        elif username and password:
            user = self.authenticate(username=username, password=password)
        elif phone_number and password:
            user = self.authenticate(
                phone_number=phone_number, password=password)
        else:
            msg = _(
                'Must include either "username", "email" or "phone_number" and "password".')
            raise exceptions.ValidationError(msg)
        return user

    def validate(self, attrs):
        username = attrs.get('username')
        email = attrs.get('email')
        phone_number = attrs.get('phone_number')
        password = attrs.get('password')

        user = None

        # Authentication through email
        if app_settings.AUTHENTICATION_METHOD == app_settings.AuthenticationMethod.EMAIL:
            user = self._validate_email(email, password)

        # Authentication through username
        elif app_settings.AUTHENTICATION_METHOD == app_settings.AuthenticationMethod.USERNAME:
            user = self._validate_username_email_phone_number(
                username, email, password, phone_number)

        # Authentication through either username or email
        else:
            user = self._validate_username_email_phone_number(
                username, email, password, phone_number)

        # Did we get back an active user?
        if user:
            if not user.is_active:
                send_otp_this(user.phone_number)
                msg = _('User account is disabled.')
                raise exceptions.ValidationError(msg)
        else:
            msg = _('Unable to log in with provided credentials.')
            raise exceptions.ValidationError(msg)

        # If required, is the email verified?
        if 'rest_auth.registration' in settings.INSTALLED_APPS:
            if app_settings.EMAIL_VERIFICATION == app_settings.EmailVerificationMethod.MANDATORY:
                email_address = user.emailaddress_set.get(email=user.email)
                if not email_address.verified:
                    raise serializers.ValidationError(
                        _('E-mail is not verified.'))

        attrs['user'] = user
        return attrs


class ExtendedLoginView(LoginView):
    serializer_class = ExtendedLoginSerialiser

    def get_response(self):
        serializer_class = self.get_response_serializer()

        if getattr(settings, 'REST_USE_JWT', False):
            data = {
                'user': self.user,
                'token': self.token
            }
            serializer = serializer_class(instance=data,
                                          context={'request': self.request})
        else:
            serializer = serializer_class(instance=self.token,
                                          context={'request': self.request})

        data = serializer.data

        # if self.user.is_superuser and self.user.is_staff:
        #     data["group"] = "Admin"
        # else:
        groups = self.user.groups.all()
        if len(groups) != 1:
            print('User: ', self.user, 'Groups: ',
                    groups, 'Groups count: ', len(groups))
            data["group"] = "Unknown"
            Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        assert len(groups) == 1, "expected that a user has only one group"

        data["group"] = groups[0].name
        return Response(data, status=status.HTTP_200_OK)


try:
    from allauth.account import app_settings as allauth_settings
    from allauth.utils import (email_address_exists,
                               get_username_max_length)
    from allauth.account.adapter import get_adapter
    from allauth.account.utils import setup_user_email
except ImportError:
    raise ImportError("allauth needs to be added to INSTALLED_APPS.")
from utils.helpers import check_phone_number


class ExtendedRegisterSerializer(RegisterSerializer):
    group = serializers.CharField(write_only=True)
    first_name = serializers.CharField(write_only=True)
    last_name = serializers.CharField(write_only=True)
    second_name = serializers.CharField(write_only=True)
    phone_number = serializers.CharField(write_only=True)

    def validate_username(self, name):
        return True

    def validate_group(self, group):
        try:
            Group.objects.get(name=group)
        except Group.DoesNotExist as e:
            raise serializers.ValidationError(
                _("Invalid group name.{E}".format(E=e)))
        return group

    def validate_phone_number(self, phone_number):
        phone_number_is_valid =  check_phone_number(phone_number)
        if not phone_number_is_valid:
            raise serializers.ValidationError("Phone_number is not valid.")
        users = User.objects.filter(phone_number=phone_number)
        if users.exists():
            raise serializers.ValidationError("This phone number is already taken.")
        return phone_number

    def get_cleaned_data(self):
        return {
            'username': None,
            'password1': self.validated_data.get('password1', ''),
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'second_name': self.validated_data.get('second_name', ''),
            'phone_number': self.validated_data.get('phone_number', ''),
            'email': self.validated_data.get('email', ''),
            'group': self.validated_data.get('group', ''),
        }

    def save(self, request):
        adapter = get_adapter()
        # user = adapter.new_user(request)
        user = User()
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        setup_user_email(request, user, [])
        group = Group.objects.get(name=self.cleaned_data['group'])
        # group = Group.objects.get(name=PERMISSION_CLASSES['client'])
        group.user_set.add(user)
        user.second_name = self.cleaned_data['second_name']
        user.phone_number = self.cleaned_data['phone_number']
        user.is_active = False
        send_otp_this(user.phone_number)
        user.save()
        return user


class ExtendedRegisterView(RegisterView):
    serializer_class = ExtendedRegisterSerializer
