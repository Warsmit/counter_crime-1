from rest_framework import permissions
from django.contrib.auth.models import (
    # User,
    Group,
    Permission,
)
from rest_framework import exceptions
from .models import User
from counter_crime.settings import PERMISSION_CLASSES

class ExtendedModelPermissions(permissions.DjangoModelPermissions):
    """
    Similar to `DjangoModelPermissions`, but adding 'view' permissions.
    """
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }


class ExtendedModelPermissionsNonAuthReadOnly(permissions.IsAuthenticatedOrReadOnly, permissions.DjangoModelPermissions):
    """
    Similar to `DjangoModelPermissions`, but adding 'view' permissions.
    """
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }


# class IsOwnerOrReadOnlyRoute(permissions.IsAuthenticatedOrReadOnly, permissions.DjangoModelPermissions):
#     """
#     Object-level permission to only allow owners of an object to edit it.
#     Assumes the model instance has an `owner` attribute.
#     """

#     def has_object_permission(self, request, view, obj):
#         # Read permissions are allowed to any request,
#         # so we'll always allow GET, HEAD or OPTIONS requests.
#         print(permissions.SAFE_METHODS)
#         print(request.method)
#         if request.method in permissions.SAFE_METHODS:
#             return True

#         # Instance must have an attribute named `guide`
#         if request.user is None:
#             return False

#         print(request.user)
#         return obj.guide == request.user or \
#             Group.objects.get(name=PERMISSION_CLASSES['admin']) in \
#                 User.objects.get(username=request.user).groups.all() \
#              or Group.objects.get(name=PERMISSION_CLASSES['moderator']) in \
#                  User.objects.get(username=request.user).groups.all()


class IsOwnerOrReadOnlyImages(permissions.DjangoModelPermissions):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `guide`
        if request.user is None:
            return False

        print(request.user)
        return obj.pk in request.user or \
            Group.objects.get(name=PERMISSION_CLASSES['admin']) in \
                User.objects.get(username=request.user).groups.all() \
             or Group.objects.get(name=PERMISSION_CLASSES['moderator']) in \
                 User.objects.get(username=request.user).groups.all()


class AddRegistrationRequests(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    # def has_object_permission(self, request, view, obj):
        # # Read permissions are allowed to any request,
        # # so we'll always allow GET, HEAD or OPTIONS requests.
        # if request.method == "POST":
        #     return True

        # if request.user is None:
        #     return False
        # return Group.objects.get(name=PERMISSION_CLASSES['admin'])  in \
        #         User.objects.get(username=request.user).groups.all() \
        #      or Group.objects.get(name=PERMISSION_CLASSES['moderator']) in \
        #          User.objects.get(username=request.user).groups.all()

    perms_map = {
        # 'GET': ['%(app_label)s.view_%(model_name)s'],
        # 'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        # 'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        # 'PUT': ['%(app_label)s.change_%(model_name)s'],
        # 'PATCH': ['%(app_label)s.change_%(model_name)s'],
        # 'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

    authenticated_users_only = False

    def get_required_permissions(self, method, model_cls):
        """
        Given a model and an HTTP method, return the list of permission
        codes that the user is required to have.
        """
        kwargs = {
            'app_label': model_cls._meta.app_label,
            'model_name': model_cls._meta.model_name
        }

        if method not in self.perms_map:
            raise exceptions.MethodNotAllowed(method)

        return [perm % kwargs for perm in self.perms_map[method]]

    def _queryset(self, view):
        assert hasattr(view, 'get_queryset') \
            or getattr(view, 'queryset', None) is not None, (
            'Cannot apply {} on a view that does not set '
            '`.queryset` or have a `.get_queryset()` method.'
        ).format(self.__class__.__name__)

        if hasattr(view, 'get_queryset'):
            queryset = view.get_queryset()
            assert queryset is not None, (
                '{}.get_queryset() returned None'.format(view.__class__.__name__)
            )
            return queryset
        return view.queryset

    def has_permission(self, request, view):
        # Workaround to ensure DjangoModelPermissions are not applied
        # to the root view when using DefaultRouter.
        if getattr(view, '_ignore_model_permissions', False):
            return True

        # if not request.user or (
        #    not request.user.is_authenticated and self.authenticated_users_only):
        #     return False

        queryset = self._queryset(view)
        perms = self.get_required_permissions(request.method, queryset.model)

        return perms

