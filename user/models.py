from django.contrib.auth.models import AbstractUser
from django.db import models
from utils.validators import NameValidator
from fernet_fields import EncryptedCharField, EncryptedDateField
from utils.validators import PhoneValidator
from django.contrib.auth import get_user_model

class User(AbstractUser):
    """
    Model for storing information about users
        fields:
            first_name -> first name
            last_name -> last name
            secood_name -> second name
            updated -> date and time of updation
            birthday -> date of birth

    """
    # image = models.ImageField(upload_to='images', blank=True)
    phone_number = models.CharField(
        verbose_name="Номер телефона", max_length=13, blank=False, null=False, validators=[PhoneValidator()], unique=True)
    first_name = EncryptedCharField(verbose_name="Имя", max_length=100, blank=False, null=False, validators=[NameValidator()])
    last_name = EncryptedCharField(verbose_name="Фамилия", max_length=100, blank=False, null=False, validators=[NameValidator()])
    second_name = EncryptedCharField(verbose_name="Отчетство", max_length=100, blank=False, null=False, validators=[NameValidator()])
    updated = models.DateTimeField(
        verbose_name='Updated', auto_now_add=False, auto_now=True,)
    birthday = EncryptedDateField(verbose_name="Дата Рождения", blank=True, null=True)

    def __str__(self):
        return '{} {} {} {} {}'.format( self.first_name, self.last_name, self.second_name, self.phone_number, self.email,)

class PhoneOTP(models.Model):
    phone_number = models.CharField(
        validators=[PhoneValidator], max_length=13, unique=True)
    otp = models.CharField(max_length=9, blank=True, null=True)
    count = models.IntegerField(default=0, help_text='Number of otp sent')
    logged = models.BooleanField(
        default=False, help_text='If otp verification got successful')
    forgot = models.BooleanField(
        default=False, help_text='only true for forgot password')
    forgot_logged = models.BooleanField(
        default=False, help_text='Only true if validdate otp forgot get successful')

    def __str__(self):
        return str(self.phone_number) + ' is sent ' + str(self.otp)