#!/bin/bash

# This script install all modules and dependesies, migrate and start server

VERSION="0.0.1"

# load common functions
. common_bash_commands

case "$OSTYPE" in
    msys)
        PYTHON=python
        PIP="python -m pip"
        ;;
    linux-gnueabihf)
        PYTHON=python3.8
        PIP=pip3.8
        ;;
    linux-gnu)
        PYTHON=python3
        PIP=pip3
        ;;
esac

build
rm -r -f db.sqlite3
migrate
$PYTHON manage.py createsuperuser 
$PYTHON grant_permissions.py
$PYTHON manage.py runserver 127.0.0.1:${1:-8000}
