from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    AmmoTypeList,
    AmmoTypeDetail,
)


urlpatterns = [
    url(r'^ammo_types/$', AmmoTypeList.as_view(), name='ammotype-list'),
    url(r'^ammo_types/(?P<pk>[0-9]+)$', AmmoTypeDetail.as_view(), name='ammotype-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
